package ru.dokwork.cucumber;

import cucumber.api.java.en.Given;

import java.util.List;

public class ExampleSteps {
    @Given("^objects with id and name$")
    public void objects_with_id_and_name(List<CustomObject> objects) throws Throwable {
        assert objects.size() > 0;
    }
}
