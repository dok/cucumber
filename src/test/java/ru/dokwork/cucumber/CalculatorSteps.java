package ru.dokwork.cucumber;

import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.Тогда;
import org.junit.Assert;

public class CalculatorSteps {

    private Calculator calc;

    double a;
    double b;
    double result;

    @Дано("^дано два числа (-?\\d+.?\\d*) и (-?\\d+.?\\d*)")
    public void given(double a, double b) {
        this.a = a;
        this.b = b;
        this.calc = new Calculator();
    }

    @Если("^сложить их")
    public void when_sum() {
        result = calc.sum(a, b);
    }

    @Если("^перемножить их")
    public void when_pow() {
        result = calc.pow(a, b);
    }

    @Тогда("^получим (-?\\d+.?\\d*)")
    public void then(double res) {
        Assert.assertEquals(res, result, 0.0001);
    }
}