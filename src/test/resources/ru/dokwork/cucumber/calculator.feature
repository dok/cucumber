  # language: ru

  Функционал: Calculator

    Структура сценария: Суммирование двух чисел
    Допустим дано два числа <a> и <b>
    Если сложить их
    То получим <c>

    Примеры:
    | a  | b  | c  |
    | 3  | 2  | 5  |
    |-13 | 91 | 78 |

    Структура сценария: Перемножение двух чисел
    Допустим дано два числа <a> и <b>
    Если перемножить их
    То получим <c>

    Примеры:
    | a  | b  | c  |
    | 3  | 2  | 6  |
    |-1  | 4  | -4 |