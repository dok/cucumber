package ru.dokwork.cucumber;

public class CustomObject {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public CustomObject(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
